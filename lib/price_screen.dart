import 'package:bitcoin_ticker/network.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io' show Platform;

import 'coin_data.dart';

const String api = 'https://apiv2.bitcoinaverage.com/indices/global/ticker/BTC';

class PriceScreen extends StatefulWidget {
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {

  NetworkHelper nhelper = NetworkHelper();
  String selectedCurrency = 'USD';
  String bitcoinValue = "?";

  DropdownButton getAndroidDropDown() {
    List<DropdownMenuItem<String>> dropdownList = [];
    for(int i = 0; i < currenciesList.length; i++){

      String currency = currenciesList[i];
      var newItem = DropdownMenuItem(
        child: Text(currency),
        value: currency,
      );
      dropdownList.add(newItem);
    }

    return DropdownButton<String>(
        value: selectedCurrency,
        onChanged: (value){
          setState(() {
            selectedCurrency = value;
          });

          getValues();
        },
        items: dropdownList
    );
  }

  CupertinoPicker getIosPicker(){
    List<Widget> pickerList = [];

    for(String currency in currenciesList){
      var item = Text(currency);
      pickerList.add(item);
    }

    return CupertinoPicker(
      backgroundColor: Colors.lightBlue,
      itemExtent: 32.0,
      onSelectedItemChanged: (index){
        print(index);
      },
      children: pickerList,
    );
  }

  Widget getPicker(){
    if(Platform.isIOS){
      return getIosPicker();
    }
    else if(Platform.isAndroid){
      return getAndroidDropDown();
    }
  }

  void getValues() async {

    var value = await nhelper.getData(currency: selectedCurrency);
    var btcValue = value["last"];

    setState(() {
      bitcoinValue = btcValue.toString();
    });

  }


  @override
  void initState() {
    super.initState();

    getValues();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('🤑 Coin Ticker'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 0),
            child: Card(
              color: Colors.lightBlueAccent,
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
                child: Text(
                '1 BTC = $bitcoinValue $selectedCurrency',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
              ),
            ),
          ),
          Container(
            height: 150.0,
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: 30.0),
            color: Colors.lightBlue,
//            child: getPicker()
          child: Platform.isAndroid ? getAndroidDropDown() : getIosPicker(),

          ),
        ],
      ),
    );
  }
}
