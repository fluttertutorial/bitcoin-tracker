import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class NetworkHelper {

  String api = 'https://apiv2.bitcoinaverage.com/indices/global/ticker/BTCCAD';

  Future getData({@required String currency}) async {

    print(api);
    http.Response res = await http.get(api);

    if(res.statusCode == 200) {
      var bitcoinData = jsonDecode(res.body);
      return bitcoinData;
    }
    else {
      print(res.statusCode);
    }
  }
}